# Readme

## About genpylib

**genpylib** is a set of Python libraries containing general purpose functions which shared between several Python codes. Among many other functions, it contains:

* cart2sph/sph2cart: to transform cartesian coordinates to spherical coordinates (and vice versa)
* rebatan: unwrap the phase obtained by using the arctan2 function
* sequify: create a uniform/non-uniform grid of integer/floats from a simple string variable
* sliceprocs: distribute a number of elements among the processors (for parallelization purposes)
* comonstr: find the common substrings in a list of strings
* rotmat: generate the rotation matrix
* fitbands: extract the time delays from the sidebands of a RABBIT spectrum by using a fitting function or Fast Fourier Transform

## Installation

Requirements:
1. Python 3.0 and higher versions
2. Numpy
3. Matplotlib
4. Scipy
