
def getnerfmt(vec1, vec2, vec3):
    
    # Generates the Natural Extension Reference Frame (nerfmt) from the vectors defined by the vectors vec1, vec2 and vec3 (see Parsons et al, Journal of Comp. Chem., 26(10):1063-1068, 2005)
    
    import numpy as np
    
    nerfmt = np.zeros((3,3))
    
    vtp1 = vec3-vec2
    nerfmt[:,0] = vtp1/np.linalg.norm(vtp1)
    
    vtp1 = vec2-vec1
    vtp2 = np.cross(vtp1, nerfmt[:,0])
    nerfmt[:,2] = vtp2/np.linalg.norm(vtp2)
    
    nerfmt[:,1] = np.cross(nerfmt[:,2], nerfmt[:,0])
    
    return nerfmt

def zmat2cart(zmat, conc, vor1=[0., 0., 1.], vor2=[1., 1., 0.]):
    # By default, we adpot the convention where atom1 lies in the center, atom2 is located on the z-axis and atom3 in the x,z plane
    
    import numpy as np
    
    deg2rad = np.pi/180.
    
    vecd = np.zeros(3)
    
    von1 = np.array(vor1)
    von1 = vor1/np.linalg.norm(von1)
    von2 = np.array(vor2)
    von2 = vor2/np.linalg.norm(von2)
    
    print("yo", von1, von2)
    
    nat = len(zmat)
    cart = np.zeros((nat,3))
    cart[1,:] = zmat[1,0]*von1
    cart[2,:] = zmat[2,0]*von2
    
    for i in range(3,nat):
        theta=np.pi-zmat[i,1]*deg2rad
        phi=zmat[i,2]*deg2rad
        vecd[0] = zmat[i,0]*np.cos(theta)
        vecd[1] = zmat[i,0]*np.sin(theta)*np.cos(phi)
        vecd[2] = zmat[i,0]*np.sin(theta)*np.sin(phi)
        
        nerfmt = getnerfmt(cart[conc[i][2]-1,:], cart[conc[i][1]-1,:], cart[conc[i][0]-1,:])
        cart[i,:] = np.dot(nerfmt,vecd)+cart[conc[i][0]-1,:]
    
    return cart

def concinit(cart):
    
    conc = []
    nat = len(cart)
    for i in range(nat):
        conc.append([1, 2, 3])
    
    return conc

def cart2zmat(cart, conc, frmt=0):
    
    import numpy as np
    
    nat = len(cart)
    if frmt == 0:
        zmat = []
        for i in range(nat):
            intcoo = zmatcoo(cart, conc[i], i)
            line = str(i+1)
            for j in range(len(conc[i])):
                line = line+" "+str(conc[i][j])+" "+"{:.4f}".format(intcoo[j])
            zmat.append(line)
    else:
        zmat = np.zeros((nat, 3))
        for i in range(nat):
            intcoo = zmatcoo(cart, conc[i], i)
            for j in range(len(conc[i])):
                zmat[i,j] = intcoo[j]
        
    return zmat

def zmatcoo(cart, conc, iat):
    
    import numpy as np
    import math
    
    rad2deg = 180./np.pi
    
    nc = len(conc)
    conc = [i-1 for i in conc]
    
    res = []
    if nc > 0:
        print(cart[conc[0]], cart[iat])
        print(cart[conc[0]]-cart[iat])
        res.append(np.linalg.norm(cart[conc[0]]-cart[iat]))
        
    if nc > 1:
        u1 = cart[iat]-cart[conc[0]]
        u2 = cart[conc[1]]-cart[conc[0]]

        n1 = np.cross(u1, u2)
        ax = np.linalg.norm(n1)
        ay = np.dot(u1, u2)

        res.append(np.arctan2(ax, ay)*rad2deg)

    if nc > 2:
    
        b1 = cart[conc[0]]-cart[iat]
        b2 = cart[conc[1]]-cart[conc[0]]
        b3 = cart[conc[2]]-cart[conc[1]]

        n1 = np.cross(b1, b2)
        n2 = np.cross(b2, b3)
        n1 = n1/np.linalg.norm(n1)
        n2 = n2/np.linalg.norm(n2)
        n3 = np.cross(n1,n2)

        ax = np.dot(n3, b2)/np.linalg.norm(b2)
        ay = np.dot(n1, n2)
        res.append(np.arctan2(ax, ay)*rad2deg)
    
    for i in range(nc):
        if res[i] < 0.:
            res[i] = res[i]+360.
    
    return res

def printlcval(mesh,i1,i2,i3,i4):
    n1, n2, n3, n4 = mesh.shape
    ln = i2*n3*n4+i3*n4+i4
    cn = ln%6+1
    ln = ln/6+14
    print(mesh[i1,i2,i3,i4], ln, cn)


class cubclass(object):
    
    def __init__(self, fname="none"):
        self.fname = fname

    def cubread(self, verbose=0):
        
        import sys, os
        import numpy as np
        
        if self.fname == "none":
            print("Error! The present object has no filename and cannot be assigned from file!")
            sys.exit()
        
        ext = self.fname.split('/')[-1].split('.')[-1]
        if ext.lower() != "cub":
            print("Error! Bad extension for cubfile :", ext)
            sys.exit()
        
        xyzmin = np.zeros(3)
        nv = np.zeros(3, dtype=np.int)
        step = np.zeros((3,3))
        
        with open(self.fname, 'r') as f:
            for i in range(2): f.readline()
            
            nat, xyzmin[0], xyzmin[1], xyzmin[2] = f.readline().split()
            nat = int(nat)
            
            for i in range(3): 
                nv[i], step[i,0], step[i,1], step[i,2] = f.readline().split()
                if np.sum(step[i,:]) != step[i,i]:
                    print("Error! Non-cubic framework: case not implemented yet!")
                    sys.exit()
            
            atnum = np.zeros(nat,dtype=int)
            atgeo = np.zeros((nat,3))
            for i in range(nat):
                atnum[i], dummy, atgeo[i,0], atgeo[i,1], atgeo[i,2] = f.readline().split()
                atnum[i] = int(atnum[i])
            #nm = sum(1 for dummy in f.read().split())
            
            self.mesh = np.zeros((nv[0],nv[1],nv[2]))
            self.mesh[:,:,:] = np.array([ list(map(float,dummy.replace("D", "E").split())) for dummy in f.read().split() ]).reshape((nv[0],nv[1],nv[2]))
            
        f.close()
        
        if verbose == 1:
            print()
            print("Filename: ", self.fname)
            print("Nb of atoms: ", nat)
            print("Origin: ", xyzmin[:])
            print("Step axis vectors:")
            for i in range(3): print(step[i,:])
            print("Nb of points in each direction: ", nv[:])
            print("Atomic numbers: ", atnum[:])
            print("Geometry:")
            for i in range(nat): print(atgeo[i,:])
            print("Number of elements in the grid: ", nv[0]*nv[1]*nv[2])
        
        self.nat = nat
        self.xyzmin = xyzmin
        self.step = step
        self.nv = nv
        self.atgeo = atgeo
        self.atnum = atnum
    
    def cubinit(self,nat,xyzmin,step,nv,atgeo,atnum):
        
        import numpy as np
        from tuttilib import testreptyp
        
        if testreptyp(nat) != 1 or len(xyzmin) != 3 or step.shape != (3,3) or len(nv) != 3 or len(atnum) != nat or atgeo.shape != (nat,3):
            print("Error! At least one attribute do not have the good format!")
            sys.exit()
        
        self.nat = nat
        self.xyzmin = xyzmin
        self.step = step
        self.nv = nv
        self.atgeo = atgeo
        self.atnum = atnum
        self.mesh = np.zeros((nv[0],nv[1],nv[2]))
        
    def cubgetpar(self):
        
        return self.nat, self.xyzmin, self.step, self.nv, self.atnum, self.atgeo
        
    def cubwrite(self,fname):
    
        f = open(fname, 'w')
        
        f.write(fname+"\n")
        f.write("writecubfile"+"\n")
        print("{:6}".format(self.nat), end=' ', file=f)
        print(("{:12.6f}"*3).format(*self.xyzmin), file=f)
        #printvec(self.xyzmin,f)
        for i in range(3):
            print("{:6}".format(self.nv[i]), end=' ', file=f)
            print(("{:12.6f}"*3).format(*self.step[i,:]), file=f)
            #printvec(self.step[i,:],f)
        for i in range(self.nat):
            print("{:6}".format(self.atnum[i]), end=' ', file=f)
            print(("{:12.6f}"*4).format(0.0, *self.atgeo[i,:]), file=f)
            #printvec(self.atgeo[i,:],f)
        
        l = 0
        line = ""
        
        for i in range(self.nv[0]):
            for j in range(self.nv[1]):
                for k in range(self.nv[2]):
                    l += 1
                    print("{:12.5e}".format(self.mesh[i,j,k]), end=' ', file=f)
                    if l%6 == 0:
                        print(file=f)

        f.close()
    
    
    def cubplot(self,isoval,defmcub,pfig=None):
        
        from mpl_toolkits.mplot3d import Axes3D
        from mpl_toolkits.mplot3d.art3d import Poly3DCollection
        import matplotlib.pyplot as plt
        from skimage import measure
        import numpy as np
        
        xyzmax = np.zeros(3)
        for i in range(3): xyzmax[i] = self.xyzmin[i]+(self.nv[i]-1)*self.step[i,i]
    
        if pfig is None:
            fig = plt.figure(figsize=(10, 12))
        else:
            fig = pfig
        
        ax = fig.add_subplot(111, projection='3d')
        #ax.set_facecolor('black')
        
        p3dcolor = ["b", "r"]
        for i in range(2):
            verts, faces, normals, values = measure.marching_cubes((1.0+i*(-2.0))*self.mesh, isoval, spacing=(self.step[0,0],self.step[1,1],self.step[2,2]),step_size=defmcub)
            for j in range(3):
                verts[:,j] = verts[:,j]+self.xyzmin[j]
            isosurf = Poly3DCollection(verts[faces], alpha=0.25, linewidth=0.1, edgecolors='#000000')
            isosurf.set_facecolor(p3dcolor[i])
            ax.add_collection3d(isosurf)

        #drawmol(self.atnum,self.atgeo,ax)
        ax.set_xlim(self.xyzmin[0], xyzmax[0])  
        ax.set_ylim(self.xyzmin[1], xyzmax[1])  
        ax.set_zlim(self.xyzmin[2], xyzmax[2])
        ax.set_xlabel("x")
        ax.set_ylabel("y") 
        ax.set_zlabel("z") 
        #ax.axis("off")
        
        if pfig is None:
            plt.show()

    
def drawmol(atnum,atgeo,objax):
    
    import openbabel
    import numpy as np
    
    nt = 10
    r0 = 0.8
    rf = 0.2
    nat = len(atnum)
    
    for i in range(nat):
        rgb = openbabel.etab.GetRGB(atnum[i])
        rad = r0 + rf*openbabel.etab.GetCovalentRad(atnum[i])
        u, v = np.mgrid[0:2*np.pi:(2*nt*1j), 0:np.pi:(nt*1j)]
        x = rad*np.cos(u)*np.sin(v)+atgeo[i,0]
        y = rad*np.sin(u)*np.sin(v)+atgeo[i,1]
        z = rad*np.cos(v)+atgeo[i,2]
        objax.plot_surface(x, y, z, alpha=1, linewidths=0, color=rgb)

def atnum(label):
    atnumdic = {"H": 1, "He": 2, "Li": 3, "Be": 4, "B": 5, "C": 6, "N": 7, "O": 8, "F": 9, "Ne": 10, "Na": 11, "Mg": 12, "Al": 13, "Si": 14, "P": 15, "S": 16, "Cl": 17, "Ar": 18, "K": 19, "Ca": 20, "Sc": 21, "Ti": 22, "V": 23, "Cr": 24, "Mn": 25, "Fe": 26, "Co": 27, "Ni": 28, "Cu": 29, "Zn": 30, "Ga": 31, "Ge": 32, "As": 33, "Se": 34, "Br": 35, "Kr": 36, "Rb": 37, "Sr": 38, "Y": 39, "Zr": 40, "Nb": 41, "Mo": 42, "Tc": 43, "Ru": 44, "Rh": 45, "Pd": 46, "Ag": 47, "Cd": 48, "In": 49, "Sn": 50, "Sb": 51, "Te": 52, "I": 53, "Xe": 54, "Cs": 55, "Ba": 56, "La": 57, "Ce": 58, "Pr": 59, "Nd": 60, "Pm": 61, "Sm": 62, "Eu": 63, "Gd": 64, "Tb": 65, "Dy": 66, "Ho": 67, "Er": 68, "Tm": 69, "Yb": 70, "Lu": 71, "Hf": 72, "Ta": 73, "W": 74, "Re": 75, "Os": 76, "Ir": 77, "Pt": 78, "Au": 79, "Hg": 80, "Tl": 81, "Pb": 82, "Bi": 83, "Po": 84, "At": 85, "Rn": 86, "Fr": 87, "Ra": 88, "Ac": 89, "Th": 90, "Pa": 91, "U": 92, "Np": 93, "Pu": 94, "Am": 95, "Cm": 96, "Bk": 97, "Cf": 98, "Es": 99, "Fm": 100, "Md": 101, "No": 102, "Lr": 103, "Rf": 104, "Db": 105, "Sg": 106, "Bh": 107, "Hs": 108, "Mt": 109}
    
    return atnumdic[label]

def atcol(label):
    atcoldic = {"H": "FFFFFF", "He": "D9FFFF", "Li": "CC80FF", "Be": "C2FF00", "B": "FFB5B5", "C": "909090", "N": "3050F8", "O": "FF0D0D", "F": "90E050", "Ne": "B3E3F5", "Na": "AB5CF2", "Mg": "8AFF00", "Al": "BFA6A6", "Si": "F0C8A0", "P": "FF8000", "S": "FFFF30", "Cl": "1FF01F", "Ar": "80D1E3", "K": "8F40D4", "Ca": "3DFF00", "Sc": "E6E6E6", "Ti": "BFC2C7", "V": "A6A6AB", "Cr": "8A99C7", "Mn": "9C7AC7", "Fe": "E06633", "Co": "F090A0", "Ni": "50D050", "Cu": "C88033", "Zn": "7D80B0", "Ga": "C28F8F", "Ge": "668F8F", "As": "BD80E3", "Se": "FFA100", "Br": "A62929", "Kr": "5CB8D1", "Rb": "702EB0", "Sr": "00FF00", "Y": "94FFFF", "Zr": "94E0E0", "Nb": "73C2C9", "Mo": "54B5B5", "Tc": "3B9E9E", "Ru": "248F8F", "Rh": "0A7D8C", "Pd": "006985", "Ag": "C0C0C0", "Cd": "FFD98F", "In": "A67573", "Sn": "668080", "Sb": "9E63B5", "Te": "D47A00", "I": "940094", "Xe": "429EB0", "Cs": "57178F", "Ba": "00C900", "La": "70D4FF", "Ce": "FFFFC7", "Pr": "D9FFC7", "Nd": "C7FFC7", "Pm": "A3FFC7", "Sm": "8FFFC7", "Eu": "61FFC7", "Gd": "45FFC7", "Tb": "30FFC7", "Dy": "1FFFC7", "Ho": "00FF9C", "Er": "00E675", "Tm": "00D452", "Yb": "00BF38", "Lu": "00AB24", "Hf": "4DC2FF", "Ta": "4DA6FF", "W": "2194D6", "Re": "267DAB", "Os": "266696", "Ir": "175487", "Pt": "D0D0E0", "Au": "FFD123", "Hg": "B8B8D0", "Tl": "A6544D", "Pb": "575961", "Bi": "9E4FB5", "Po": "AB5C00", "At": "754F45", "Rn": "428296", "Fr": "420066", "Ra": "007D00", "Ac": "70ABFA", "Th": "00BAFF", "Pa": "00A1FF", "U": "008FFF", "Np": "0080FF", "Pu": "006BFF", "Am": "545CF2", "Cm": "785CE3", "Bk": "8A4FE3", "Cf": "A136D4", "Es": "B31FD4", "Fm": "B31FBA", "Md": "B30DA6", "No": "BD0D87", "Lr": "C70066", "Rf": "CC0059", "Db": "D1004F", "Sg": "D90045", "Bh": "E00038", "Hs": "E6002E", "Mt": "EB0026"}
    
    return atcoldic[label]

def hex2rgb(hex):
     hex = hex.lstrip('#')
     hlen = len(hex)
     hdiv = int(hlen/3)
     return tuple(int(hex[i:i+hdiv], 16)/255. for i in range(0, hlen,hdiv))

class ccamol(object):
    
    def __init__(self, fname=None):
        self.fname = fname

    def readcca(self):
        
        import numpy as np
        from mendeleev import element
        from scipy.linalg import norm
        
        #from vmd import color
        
        f = open(self.fname)
        self.nat = int(f.readline())
        f.readline()
        self.labels = ["" for i in range(self.nat)]
        self.carts = np.zeros((self.nat,3))
        self.cols = []
        self.rads = np.zeros(self.nat)
        self.rmax = 0.
        for i in range(self.nat):
            line = f.readline().split()
            self.labels[i], self.carts[i,:] = line[0], line[1:]
            at = element(self.labels[i])
            #self.cols.append(hex2rgb(atcol(self.labels[i])))
            self.cols.append(hex2rgb(at.molcas_gv_color))
            #self.cols.append(at.cpk_color)
            self.rads[i] = at.vdw_radius /element("Fr").vdw_radius
            rtot = norm(self.carts[i,:])+self.rads[i]
            if rtot > self.rmax:
                self.rmax = rtot
        self.bonds = [None for i in range(self.nat)]
        for i in range(self.nat):
            self.bonds[i] = [int(j)-1 for j in f.readline().split()]
        f.close()
        self.radc = 0.15


def draw_sphere(fpm, coo, rs, npt, alpha=1, color=(1, 1, 1), pmod="mlab", antial=False):
    
    import numpy as np
    nt = int(round(np.sqrt(npt/2.)))
    u, v = np.mgrid[0:2*np.pi:2*nt*1j, 0:np.pi:nt*1j]
    x = coo[0]+rs*np.cos(u)*np.sin(v)
    y = coo[1]+rs*np.sin(u)*np.sin(v)
    z = coo[2]+rs*np.cos(v)
    
    if pmod == "mlab":
        fpm.mesh(x, y, z, color=color, opacity=alpha)
    elif pmod == "plt":
        fpm.plot_surface(x, y, z, alpha=alpha, antialiased=antial, color=color, linewidth=0)
     

def draw_plane(fpm, vert, npt, alim=False, alpha=1, color=(1, 1, 1), pmod="mlab", antial=False):
    
    import numpy as np
    from scipy.linalg import norm
    
    vert = np.array(vert)

    # These two vectors are in the plane
    v1 = vert[2] - vert[0]
    v2 = vert[1] - vert[0]
    
    cmin, cmax = np.zeros(3), np.zeros(3)
    if alim:
        for i in range(3):
            cmax[i] = np.max(np.abs(vert[:,i]))
            cmin = -cmax
    else:
        for i in range(3):
            cmin[i] = np.min(vert[:,i])
            cmax[i] = np.max(vert[:,i])
        
    #print(cmin, cmax)

    # the cross product is a vector normal to the plane
    cp = np.cross(v1, v2)
    
    a, b, c = cp

    # This evaluates a * x3 + b * y3 + c * z3 which equals d
    d = np.dot(cp, vert[2])

    #print('The equation is {0}x + {1}y + {2}z = {3}'.format(a, b, c, d))
    x = np.linspace(cmin[0], cmax[0], npt)
    
    if c == 0.:
        z = np.linspace(cmin[2], cmax[2], npt)
        x0, z0 = np.meshgrid(x, z)
        y0 = (d-a*x0)/b
    else:
        y = np.linspace(cmin[1], cmax[1], npt)
        x0, y0 = np.meshgrid(x, y)
        z0 = (d-a*x0-b*y0)/c

    if pmod == "mlab":
        fpm.mesh(x0, y0, z0, color=color, opacity=alpha)
    elif pmod == "plt":
        fpm.plot_surface(x0, y0, z0, alpha=alpha, antialiased=antial, color=color, linewidth=0)


def draw_cylinder(fpm, ci, cf, rc, npt, alpha=1, color=(1, 1, 1), pmod="mlab", antial=False):
    
    import numpy as np
    from scipy.linalg import norm
    
    v = cf - ci
    #find magnitude of vector
    mag = norm(v)
    #unit vector in direction of axis
    v = v / mag
    #make some vector not in the same direction as v
    not_v = np.array([1, 0, 0])
    if (np.abs(v) == not_v).all():
        not_v = np.array([0, 1, 0])
    #print(v, not_v)
    #make vector perpendicular to v
    n1 = np.cross(v, not_v)
    #normalize n1
    n1 /= norm(n1)
    #make unit vector perpendicular to v and n1
    n2 = np.cross(v, n1)
    #surface ranges over t from 0 to length of axis and 0 to 2*pi
    t = np.linspace(0, mag, 2)
    theta = np.linspace(0, 2 * np.pi, npt)
    rsample = np.linspace(0, rc, 2)
    #use meshgrid to make 2d arrays
    t, theta2 = np.meshgrid(t, theta)
    
    x0, y0, z0 = [ci[i] + v[i] * t + rc * np.sin(theta2) * n1[i] + rc * np.cos(theta2) * n2[i] for i in [0, 1, 2]]
    
    rsample = np.linspace(0, rc, 2)
    rsample,theta = np.meshgrid(rsample, theta)
    
    x1, y1, z1 = [ci[i] + rsample[i] * np.sin(theta) * n1[i] + rsample[i] * np.cos(theta) * n2[i] for i in [0, 1, 2]]
    x2, y2, z2 = [ci[i] + v[i]*mag + rsample[i] * np.sin(theta) * n1[i] + rsample[i] * np.cos(theta) * n2[i] for i in [0, 1, 2]]
    
    if pmod == "mlab":
        fpm.mesh(x0, y0, z0, color=color, opacity=alpha)
        fpm.mesh(x1, y1, z1, color=color, opacity=alpha)
        fpm.mesh(x2, y2, z2, color=color, opacity=alpha)
    elif pmod == "plt":
        fpm.plot_surface(x0, y0, z0, alpha=alpha, antialiased=antial, color=color, linewidth=0)
        fpm.plot_surface(x1, y1, z1, alpha=alpha, antialiased=antial, color=color, linewidth=0)
        fpm.plot_surface(x2, y2, z2, alpha=alpha, antialiased=antial, color=color, linewidth=0)

def draw_truncone(fpm, ci, cf, r0, r1, npt, alpha=1, color=(1, 1, 1), pmod="mlab", antial=False):
    
    import numpy as np
    from scipy.linalg import norm
    
    nt = int(round(np.sqrt(npt)))
    # vector in direction of axis
    v = cf - ci
    # find magnitude of vector
    mag = norm(v)
    # unit vector in direction of axis
    v = v / mag
    # make some vector not in the same direction as v
    not_v = np.array([1, 1, 0])
    if (v == not_v).all():
        not_v = np.array([0, 1, 0])
    # make vector perpendicular to v
    n1 = np.cross(v, not_v)
    # print n1,'\t',norm(n1)
    # normalize n1
    n1 /= norm(n1)
    # make unit vector perpendicular to v and n1
    n2 = np.cross(v, n1)
    # surface ranges over t from 0 to length of axis and 0 to 2*pi
    t = np.linspace(0, mag, nt)
    theta = np.linspace(0, 2 * np.pi, nt)
    # use meshgrid to make 2d arrays
    t, theta = np.meshgrid(t, theta)
    rc = np.linspace(r0, r1, nt)
    # generate coordinates for surface
    x0, y0, z0 = [ci[i] + v[i] * t + rc * np.sin(theta) * n1[i] + rc * np.cos(theta) * n2[i] for i in [0, 1, 2]]
    
    rsample = np.linspace(0, r0, 2)
    rsample,theta = np.meshgrid(rsample, theta)
    x1, y1, z1 = [ci[i] + rsample[i] * np.sin(theta) * n1[i] + rsample[i] * np.cos(theta) * n2[i] for i in [0, 1, 2]]
    
    rsample = np.linspace(0, r1, 2)
    rsample,theta = np.meshgrid(rsample, theta)
    x2, y2, z2 = [ci[i] + v[i]*mag + rsample[i] * np.sin(theta) * n1[i] + rsample[i] * np.cos(theta) * n2[i] for i in [0, 1, 2]]
    
    if pmod == "mlab":
        fpm.mesh(x0, y0, z0, color=color, opacity=alpha)
        fpm.mesh(x1, y1, z1, color=color, opacity=alpha)
        fpm.mesh(x2, y2, z2, color=color, opacity=alpha)
    elif pmod == "plt":
        fpm.plot_surface(x0, y0, z0, alpha=alpha, antialiased=antial, color=color, linewidth=0)
        fpm.plot_surface(x1, y1, z1, alpha=alpha, antialiased=antial, color=color, linewidth=0)
        fpm.plot_surface(x2, y2, z2, alpha=alpha, antialiased=antial, color=color, linewidth=0)


def draw_arr(fpm, ci, cf, npt, pr=0.04, pw=2., ml=2., alpha=1, color=(1, 1, 1), pmod="mlab", antial=False):
    
    import numpy as np
    from scipy.linalg import norm
    
    n0 = norm(cf-ci)
    r0 = pr*n0
    pl = 3*r0*pw/n0
    if pl > 1.:
        draw_truncone(fpm, ci, cf, r0*pw, 0., npt, alpha=alpha, color=color, pmod=pmod, antial=antial)
    else:
        cm = (cf-ci)*(1.-pl)+ci
        draw_cylinder(fpm, ci, cm, r0, npt, alpha=alpha, color=color, pmod=pmod, antial=antial)
        draw_truncone(fpm, cm, cf, r0*pw, 0., npt, alpha=alpha, color=color, pmod=pmod, antial=antial)

def draw_mol(fpm, mol, rmax=1, fac=1, npt=100, alpha=1, color=(1, 1, 1), pmod="mlab", antial=False):
    
    for i in range(mol.nat):
        for j in mol.bonds[i]:
            if j >= 0 and i != j:
                medcart = (mol.carts[j]-mol.carts[i])/2.+mol.carts[i]
                draw_cylinder(fpm, mol.carts[i]*rmax, medcart*rmax, mol.radc*rmax*fac, npt, alpha=alpha,color=mol.cols[i], pmod=pmod, antial=antial)
        draw_sphere(fpm, mol.carts[i]*rmax, mol.rads[i]*rmax*fac, npt, alpha=alpha, color=mol.cols[i], pmod=pmod, antial=antial)


def initfpm(pmod):
    
    if pmod == "mlab":
        
        from mayavi import mlab as fpm
        fpm.figure(bgcolor=(0.,0.,0.))
        plt = None
    
    elif pmod == "plt":
        from matplotlib import pyplot as plt
        from mpl_toolkits.mplot3d import proj3d
        fig = plt.figure()
        fpm = fig.add_subplot(111, projection='3d')
    
    else:
        print("Error! Bad option for pmod!")
        sys.exit()
    
    return fpm, plt

def showfpm(fpm, plt, oname=None):
    
    if oname != None:
        
        if plt == None:
            fpm.savefig(filename=oname, magnification=6)
            fpm.close()
        else:
            plt.axis('off')
            plt.subplots_adjust(left=-0.15,right=1.15,top=1.15,bottom=-0.15)
            plt.savefig(oname, dpi=300, transparent=True)
            plt.close()
    else:
        if plt == None:
            fpm.show()
        else:
            plt.show()

def draw_pol(fpm, darw, rmax=1., dt=0.1, pr=0.01, color=(1.,0.,1.), text=None, pmod="mlab", antial=False):
    
    import numpy as np
    
    n0 = np.linalg.norm(darw)
    vec = np.array([1.3*float(j)*rmax/n0 for j in darw])
    draw_arr(fpm, np.zeros(3), vec, 500, pr=pr, pw=3., ml=2., alpha=1, color=color, pmod=pmod, antial=antial)
    draw_arr(fpm, np.zeros(3), -vec, 500, pr=pr, pw=3., ml=2., alpha=1, color=color, pmod=pmod, antial=antial)
    draw_text(fpm, [i+dt for i in vec], text, 0.25, alpha=1, color=color, pmod=pmod)

def draw_text(fpm, pos, text, size, alpha=1, color=(1, 1, 1), pmod="mlab"):
    if text != None:
        if pmod == "mlab":
            fpm.text3d(pos[0], pos[1], pos[2], text, scale=size, color=color, opacity=alpha)
        elif pmod == "plt":
            fpm.text(*pos, text, color=color, fontsize=90*size)
            
def draw_vec(fpm, vecs, rmax=1., dt=0.1, pr=0.01, color=None, text=None, pmod="mlab", antial=False):
    
    from matplotlib import cm, colors
    import numpy as np
    
    nv = len(vecs)
    for i in range(nv):
        n0 = np.linalg.norm(vecs[i])
        vec = [1.3*float(j)*rmax/n0 for j in vecs[i]]
        if color == None:
            col = cm.ScalarMappable(norm=colors.Normalize(vmin=0, vmax=nv-1), cmap="tab10").to_rgba(float(i/nv))[:-1]
        else:
            col = color[i]
        draw_arr(fpm, np.zeros(3), vec, 500, pr=pr, pw=3., ml=2., alpha=1, color=col, pmod=pmod, antial=antial)
        draw_text(fpm, [j+dt for j in vec], text[i], 0.25, alpha=1, color=col, pmod=pmod)
            
    

def splotmol(rtp, nk, oname=None, mol=None, vecs=None, darw=None, vscale=None, pmod="mlab", ftext=None):
    
    import sys
    import numpy as np
    from tuttilib import m_sph2cart
    from matplotlib import cm, colors

    nl = len(rtp)
    
    xyz = np.zeros((nl,3))
    xyz = m_sph2cart(rtp[:,0:3])
    
    x = xyz[:,0].reshape(nk)
    y = xyz[:,1].reshape(nk)
    z = xyz[:,2].reshape(nk)
    
    if vscale is None:
        r = rtp[:,0].reshape(nk)
    else:
        vscale = vscale/np.linalg.norm(vscale)
        r = x*vscale[0]+y*vscale[1]+z*vscale[2]
    
    cmapstr="autumn"
    cmapstr="coolwarm"
    cmapstr="jet"
    cmapstr="gnuplot"
    
    testneg = False
    if np.any(r<0):
        testneg = True
    
    antial = False
    
    fpm, plt = initfpm(pmod)
    
    if pmod == "mlab":
        
        if testneg:
            print("* Plotting pos/neg mesh...")
            cols=[(1,0,0), (0,0,1)]
            for j in (-1,1):
                p = j*np.copy(z)
                p[p < 0] = 0
                fpm.mesh(x, y, j*p, color=cols[int((j+1)/2)], opacity=0.5)
        else:
            fpm.mesh(x, y, z, colormap=cmapstr, scalars=r, opacity=0.5)
    elif pmod == "plt":
        if oname != None:
            antial = True
        import matplotlib as mpl
        from matplotlib.colors import LightSource
        from tuttilib import coldimap
        alim = max([np.max(np.abs(x)), np.max(np.abs(y)), np.max(np.abs(z))])
        fpm.set_xlim([-alim,alim])
        fpm.set_ylim([-alim,alim])
        fpm.set_zlim([-alim,alim])
        fpm.set_xlabel("x")
        fpm.set_ylabel("y")
        fpm.set_zlabel("z")
        if ftext != None:
            fpm.text(alim/5,alim/5,alim*1.2,ftext)
        fcolors = coldimap(rtp[:,0].reshape(nk),cmapstr)
        fpm.plot_surface(x, y, z,  alpha=0.4, antialiased=antial, facecolors=fcolors, linewidth=0.5, edgecolors='black')
    else:
        print("Error! Bad option for pmod!")
        sys.exit()
        
        
    rmax = np.max(rtp[:,0])
    
    if not darw is None:
        draw_pol(fpm, darw, rmax=rmax, pmod=pmod, antial=antial)
    if vecs != None:
        draw_vec(fpm, pmod, vecs, rmax=rmax, antial=antial)
    if mol != None:
        draw_mol(fpm, mol, rmax=rmax/mol.rmax, fac=0.5, npt=500, pmod=pmod, antial=antial)
    
    
    showfpm(fpm, plt, oname=None)
    
